package umb.arquetipo.verticles;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.graphql.ApolloWSHandler;
import io.vertx.ext.web.handler.graphql.GraphQLHandler;
import io.vertx.ext.web.handler.graphql.GraphiQLHandler;
import io.vertx.ext.web.handler.graphql.GraphiQLHandlerOptions;
import io.vertx.ext.web.handler.graphql.schema.VertxDataFetcher;
import umb.arquetipo.graphql.GraphQLDataFetchers;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class AppVerticle extends AbstractVerticle {
    private static final Logger logger = Logger.getLogger(AppVerticle.class);
    private static final String SUBSCRIPTION = "Subscription";
    private static final String MUTATION = "Mutation";
    private static final String QUERY = "Query";

    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_ORIGIN = "origin";
    private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = "*";
    private static final String X_REQUESTED_WITH = "x-requested-with";
    private static final String ACCEPT = "accept";
    private static final String X_PINGARUNER = "X-PINGARUNER";

    private GraphQLDataFetchers graphQLDataFetchers;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        graphQLDataFetchers = new GraphQLDataFetchers();
        graphQLDataFetchers.init(vertx, config());
    }

    @Override
    public void start() {
        createRouter()
                .map(this::setHeaderAndMethods)
                .map(this::setMainRoute)
                .map(this::setGraphiQLRoute)
                .map(this::setGraphQLRoute)
                .compose(this::createServer)
                .onFailure(e -> logger.error("Fallo al iniciar el servidor"))
                .onSuccess(r -> logger.info("Servidor iniciado"));
    }

    private Future<Router> createRouter() {
        return Future.succeededFuture(Router.router(vertx));
    }

    private Router setMainRoute(Router router) {
        StaticHandler staticHandler = StaticHandler.create("webapp");
        router.route("/").handler(BodyHandler.create());
        staticHandler.setIndexPage("source.html");
        router.route("/").handler(staticHandler);
        return router;
    }

    private Router setHeaderAndMethods(Router router) {
        router.route().handler(
                CorsHandler.create(HEADER_ACCESS_CONTROL_ALLOW_ORIGIN)
                        .allowedHeaders(getHeadersAllow())
                        .allowedMethods(getMethodsAllow()));
        return router;
    }

    private Router setGraphiQLRoute(Router router) {
        GraphiQLHandlerOptions options = new GraphiQLHandlerOptions()
                .setEnabled(config().getBoolean("GRAPHIQL"));
        router.route("/graphiql/*").handler(GraphiQLHandler.create(options));
        return router;
    }

    private Router setGraphQLRoute(Router router) {
        GraphQL graphQL = setupGraphql().result();
        router.route().handler(BodyHandler.create());
        router.route("/graphql").handler(ApolloWSHandler.create(graphQL));
        router.route("/graphql").handler(GraphQLHandler.create(graphQL));
        return router;
    }

    private Future<HttpServer> createServer(Router router) {
        HttpServerOptions httpServerOptions = new HttpServerOptions()
                .addWebSocketSubProtocol("graphql-ws");

        return Future.future(promise ->
                vertx.createHttpServer(httpServerOptions)
                        .requestHandler(router)
                        .listen(5000)
                        .onFailure(promise::fail)
                        .onSuccess(promise::complete));
    }

    private Future<String> getSchema() {
        return Future.succeededFuture(vertx.fileSystem().readFileBlocking("schema.graphqls").toString());
    }

    private TypeDefinitionRegistry parseSchema(String schema) {
        SchemaParser schemaParser = new SchemaParser();
        return schemaParser.parse(schema);
    }

    private GraphQLSchema setSchema(TypeDefinitionRegistry typeDefinitionRegistry) {
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type(QUERY, builder -> builder.dataFetcher("greet", graphQLDataFetchers.getGreet()))
                .type(QUERY, builder -> builder.dataFetcher("usuarios", graphQLDataFetchers.getOtherUser()))
                .type(QUERY, builder -> builder.dataFetcher("usuarioPorId", graphQLDataFetchers.getUsuarioPorId()))
                .type(QUERY, builder -> builder.dataFetcher("usuarioPorApellido", graphQLDataFetchers.getUsuarioPorApellido()))
                .type(QUERY, builder -> builder.dataFetcher("usuarioPorNombre", graphQLDataFetchers.getUsuarioPorNombre()))
                .type(MUTATION, builder -> builder.dataFetcher("nuevoUsuario", graphQLDataFetchers.getNewUser()))
                .build();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);
    }

    private GraphQL createGraphQL(GraphQLSchema graphQLSchema) {
        return GraphQL.newGraphQL(graphQLSchema)
                .build();
    }

    private Future<GraphQL> setupGraphql() {
        return getSchema()
                .map(this::parseSchema)
                .map(this::setSchema)
                .map(this::createGraphQL);
    }

    private Set<HttpMethod> getMethodsAllow() {
        Set<HttpMethod> methodsAllow = new HashSet<>();
        methodsAllow.add(HttpMethod.GET);
        methodsAllow.add(HttpMethod.POST);
        methodsAllow.add(HttpMethod.OPTIONS);
        methodsAllow.add(HttpMethod.DELETE);
        methodsAllow.add(HttpMethod.PATCH);
        methodsAllow.add(HttpMethod.PUT);
        return methodsAllow;
    }

    private Set<String> getHeadersAllow() {
        Set<String> headersAllow = new HashSet<>();
        headersAllow.add(X_REQUESTED_WITH);
        headersAllow.add(ACCESS_CONTROL_ALLOW_ORIGIN);
        headersAllow.add(HEADER_ORIGIN);
        headersAllow.add(HEADER_CONTENT_TYPE);
        headersAllow.add(ACCEPT);
        headersAllow.add(X_PINGARUNER);
        return headersAllow;
    }
}