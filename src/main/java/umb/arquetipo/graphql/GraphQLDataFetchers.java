package umb.arquetipo.graphql;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.handler.graphql.schema.VertxDataFetcher;
import umb.arquetipo.micro.dto.UsersDTO;
import umb.arquetipo.micro.user.Users;

import java.util.List;

public class GraphQLDataFetchers {


    Vertx vertx;
    private Users users;

    public void init(Vertx vertx, JsonObject configuration) {
        this.vertx = vertx;
        users = Users.createProxy(vertx, "user-service");
    }

    private final VertxDataFetcher<String> greet = VertxDataFetcher.create((env, handler) -> {
        handler.handle(Future.succeededFuture("Hola"));
    });

    public VertxDataFetcher<Integer> getNewUser() {
        return newUser;
    }

    private final VertxDataFetcher<Integer> newUser = VertxDataFetcher.create((env, handler) -> {
        JsonObject arguments = new JsonObject(env.getArguments());
        this.users.nuevoUsuario(arguments).onComplete(handler);
    });

    public VertxDataFetcher<List<UsersDTO>>
    getOtherUser() {
        return otherUser;
    }

    private final VertxDataFetcher<List<UsersDTO>> otherUser = VertxDataFetcher.create((env, handler) -> {

        this.users.usuarios().onComplete(handler);

    });

    public VertxDataFetcher<UsersDTO> getUsuarioPorId() {
        return usuarioPorId;
    }

    private final VertxDataFetcher<UsersDTO> usuarioPorId = VertxDataFetcher.create((env, handler) -> {
        JsonObject arguments = new JsonObject(env.getArguments());
        this.users.usuarioPorId(arguments).onComplete(handler);

    });

    public VertxDataFetcher<String> getGreet() {
        return greet;

    }

    public VertxDataFetcher<List<UsersDTO>> getUsuarioPorApellido() {
        return usuarioPorApellido;
    }

    private final VertxDataFetcher<List<UsersDTO>> usuarioPorApellido = VertxDataFetcher.create((env, handler) -> {
        JsonObject arguments = new JsonObject(env.getArguments());
        this.users.usuarioPorApellido(arguments).onComplete(handler);
    });

    public VertxDataFetcher<List<UsersDTO>> getUsuarioPorNombre() {
        return usuarioPorNombre;
    }

    private final VertxDataFetcher<List<UsersDTO>> usuarioPorNombre = VertxDataFetcher.create((env, handler) -> {
        JsonObject arguments = new JsonObject(env.getArguments());
        this.users.usuarioPorNombre(arguments).onComplete(handler);
    });

}
